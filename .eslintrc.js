module.exports = { 
  "extends": "airbnb-base",
  "rules": {
    "no-underscore-dangle": "off",
    "class-methods-use-this": "off",
    "max-len": ["error", { "code": 120 }],
    "no-param-reassign": "off",
    "no-nested-ternary": "off",
    "no-useless-constructor": "off"
  },
  "settings": {
      "resolve": {
        "node": {
         "paths": ["src", "."],
      }
    }
  }
};