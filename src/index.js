const { Client } = require('klasa');
const { config, token } = require('./config');

class ValentineClient extends Client {
  constructor(...args) {
    super(...args);
  }
}

ValentineClient.defaultGuildSchema
  .add('minAccAge', 'integer', { default: 1800000 })
  .add('roles', (schema) => schema.add('muted', 'role'))
  .add('channels', (schema) => schema.add('logs', 'TextChannel'))
  .add('channels', (schema) => schema.add('purge', 'TextChannel'), { array: true })
  .add('guildBlacklist', 'guild', { array: true });
ValentineClient.defaultClientSchema
  .add('userBlacklist', 'user', { array: true });

new ValentineClient(config).login(token);
