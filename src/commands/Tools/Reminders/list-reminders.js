
const { Command, RichDisplay } = require('klasa');
const { MessageEmbed } = require('discord.js');
const moment = require('moment');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      description: 'lists all reminders applicable to user.',
      aliases: ['reminders', 'ls-reminders'],
    });
  }

  async run(msg) {
    let reminders = this.client.schedule.tasks;

    reminders = reminders.filter(
      (reminder) => reminder.data.user === msg.author.id && reminder.taskName === 'reminder',
    );

    if (reminders.length === 0) return msg.send('You have no reminders.');

    const display = new RichDisplay(new MessageEmbed()
      .setColor(0xFFFFFF)
      .setTitle('Reminders'));

    const chunked = await this.chunkArray(reminders, 10);

    chunked.forEach((chunk) => {
      display.addPage((template) => template.setDescription(chunk.map((reminder) => {
        const { time } = reminder;
        return `\`${reminder.id}\` ${moment(time).fromNow()} (${moment(time).format('YYYY-MM-DD HH:mm Z')})`;
      })));
    });

    return display.run(await msg.send('Finding reminders...'));
  }

  async chunkArray(array, length) {
    const chunks = [];
    let i = 0;
    const n = array.length;

    while (i < n) {
      chunks.push(array.slice(i, i += length));
    }

    return chunks;
  }
};
