
const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');
const moment = require('moment');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      description: 'fetches a reminder by id.',
      usage: '<id:...string>',
      aliases: ['reminder'],
    });
  }

  async run(msg, [id]) {
    const reminder = this.client.schedule.get(id);
    if (!reminder) {
      throw String('That reminder does not exist.');
    } else if (reminder.data.user !== msg.author.id) {
      throw String('Sorry, you don\'t have a reminder with that id.');
    }

    const embed = new MessageEmbed();
    const botColor = await msg.guild.member(this.client.user).displayColor;
    const fromNow = moment(reminder.time).fromNow(true);
    const endDate = moment(reminder.time).format('ddd, MMM Do, YYYY HH:mm UTC');
    embed.addField('Reminder ID', `\`${id}\``);
    embed.addField('Content', reminder.data.text);
    embed.addField('Expires', `${fromNow} (${endDate})`);
    embed.setColor(botColor);

    msg.send(embed);
  }
};
