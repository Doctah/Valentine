
const { Command } = require('klasa');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      description: 'deletes a reminder by id.',
      usage: '<id:...string>',
      aliases: ['del-reminder', 'rm-reminder', 'remove-reminder'],
    });
  }

  async run(msg, [id]) {
    const reminder = await this.client.schedule.get(id);
    console.log(id);
    if (!reminder) throw String('That reminder does not exist.');
    if (reminder.data.user !== msg.author.id) throw String('You can\'t delete reminders that aren\'t yours.');
    reminder.delete()
      .then(() => msg.reply('the reminder has been deleted.'));
  }
};
