const { Command, Timestamp } = require('klasa');
const { MessageEmbed } = require('discord.js');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      description: 'Get information on a mentioned member.',
      usage: '[member:membername]',
      aliases: ['whois', 'who'],
    });
    this.statuses = {
      online: 'Online',
      idle: 'Idle',
      dnd: 'Do Not Disturb',
      offline: 'Offline',
    };
    this.timestamp = new Timestamp('d MMMM YYYY');
  }

  run(msg, [member = msg.member]) {
    return msg.sendEmbed(new MessageEmbed()
      .setColor(member.displayHexColor || 0xFFFFFF)
      .setAuthor(`${member.user.tag} (${member.user.id})`, member.user.displayAvatarURL())
      .addField('Discord Join Date', this.timestamp.display(member.user.createdAt), true)
      .addField('Server Join Date', this.timestamp.display(member.joinedTimestamp), true)
      .addField('Status', this.statuses[member.presence.status], true)
      .addField('Playing', member.presence.activity ? member.presence.activity.name : 'N/A', true)
      .addField('Highest Role', member.roles.size > 1 ? member.roles.highest : 'None', true)
      .addField('Hoist Role', member.roles.hoist ? member.roles.hoist : 'None', true));
  }
};
