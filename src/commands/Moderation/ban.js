
const { Command } = require('klasa');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      permissionLevel: 6,
      requiredPermissions: ['BAN_MEMBERS'],
      runIn: ['text'],
      description: 'Bans a mentioned user. Currently does not require reason (no mod-log).',
      usage: '<member:user> [reason:...string]',
      usageDelim: ' ',
    });
  }

  async run(msg, [user, reason]) {
    if (user.id === msg.author.id) throw String('Why would you ban yourself?');
    if (user.id === this.client.user.id) throw String('Have I done something wrong?');

    const member = await msg.guild.members.fetch(user).catch(() => null);
    const userRole = member.roles.highest.position;
    const authorRole = msg.member.roles.highest.position;
    if (member) {
      if (userRole >= authorRole) throw String('You cannot ban this user.');
      if (!member.bannable) throw String('I cannot ban this user.');
    }

    const options = {};
    if (reason) options.reason = reason;

    await msg.guild.members.ban(user, options);
    return msg.send(`${member.user.tag} got banned.${reason ? ` With reason of: ${reason}` : ''}`);
  }
};
