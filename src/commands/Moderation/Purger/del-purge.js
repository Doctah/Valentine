
const { Command } = require('klasa');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      description: 'deletes a purge task by id or channel.',
      usage: '<channel:channel>',
      aliases: ['del-purge', 'rm-purge', 'remove-purge'],
    });
  }

  async run(msg, [channel]) {
    const purgeTasks = this.client.schedule.tasks.filter(
      (task) => task.data.channel === channel.id && task.taskName === 'purge',
    );

    if (purgeTasks.length < 1) {
      throw String('There is no purge task for that channel.');
    }

    purgeTasks.forEach((task) => task.delete());

    msg.send(`Purge task deleted for ${channel}.`);
  }
};
