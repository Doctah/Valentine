
const { Command } = require('klasa');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      description: 'runs the purge task for the channel and resets the timer.',
      usage: '<channel:channel>',
    });
  }

  async run(msg, [channel]) {
    const purgeTasks = this.client.schedule.tasks.filter(
      (task) => task.data.channel === channel.id && task.taskName === 'purge',
    );

    if (purgeTasks.length < 1) {
      throw String('There is no purge task for that channel.');
    }

    purgeTasks.forEach((task) => {
      const date = new Date();
      task.run();
      task.update({ time: `${date.getHours()} ${date.getMinutes()} */2 * *` });
    });

    msg.send(`Purge task executed for ${channel}.`);
  }
};
