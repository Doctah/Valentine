
const { Command } = require('klasa');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      description: 'adds a purge task for this server.',
      usage: '<channel:channel>',
    });
  }

  async run(msg, [channel]) {
    const purgeTasks = this.client.schedule.tasks.filter(
      (task) => task.data.channel === channel.id && task.taskName === 'purge',
    );

    if (purgeTasks.length > 0) {
      throw String('You cannot have multiple purge tasks running on the same channel.');
    }
    const date = new Date();

    await this.client.schedule.create('purge', `${date.getHours()} ${date.getMinutes()} */2 * *`, {
      data: {
        guild: msg.guild.id,
        channel: channel.id,
      },
    });

    msg.send(`Purge task has been created for ${channel} recurring every 48 hours.`);
  }
};
