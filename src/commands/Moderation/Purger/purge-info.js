
const { Command } = require('klasa');
const { MessageEmbed } = require('discord.js');
const moment = require('moment');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      description: 'grabs list of purge tasks in current guild.',
      usage: '',
    });
  }

  async run(msg) {
    const purgeTasks = this.client.schedule.tasks.filter(
      (task) => task.taskName === 'purge' && task.data.guild === msg.guild.id,
    );

    const embed = new MessageEmbed();
    let description;

    if (purgeTasks.length > 0) {
      description = purgeTasks.map((task) => {
        const time = `${moment(task.time).format('YYYY-MM-DD HH:mm Z')}`;
        return `${this.client.channels.get(task.data.channel)} - Executes @ ${time}`;
      }).join('\n');
    } else {
      description = 'There are no purge tasks for this server.';
    }

    embed.description = description;
    embed.setAuthor(`Purge tasks in ${msg.guild.name}`, msg.guild.iconURL());
    embed.setColor(msg.guild.member(this.client.user).displayColor);
    msg.send(embed);
  }
};
