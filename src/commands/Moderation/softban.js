
const { Command } = require('klasa');

module.exports = class extends Command {
  constructor(...args) {
    super(...args, {
      permissionLevel: 6,
      requiredPermissions: ['BAN_MEMBERS'],
      runIn: ['text'],
      description: 'Softbans a mentioned user. Currently does not require reason (no mod-log).',
      usage: '<member:user> [days:int{1,7}] [reason:...string]',
      usageDelim: ' ',
    });
  }

  async run(msg, [user, days = 1, reason]) {
    if (user.id === msg.author.id) {
      throw String('Why would you ban yourself?');
    } else if (user.id === this.client.user.id) {
      throw String('Have I done something wrong?');
    }

    const member = await msg.guild.members.fetch(user).catch(() => null);

    if (member) {
      if (member.roles.highest.position >= msg.member.roles.highest.position) {
        throw String('You cannot ban this user.');
      } else if (!member.bannable) {
        throw String('I cannot ban this user.');
      }
    }

    const options = { days };
    if (reason) options.reason = reason;

    await msg.guild.members.ban(user, options);
    await msg.guild.members.unban(user, 'Softban released.');
    return msg.send(`${member.user.tag} got softbanned.${reason ? ` With reason of: ${reason}` : ''}`);
  }
};
