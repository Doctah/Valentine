/* eslint-disable no-await-in-loop */

const { Task } = require('klasa');
const { performance } = require('perf_hooks');

module.exports = class extends Task {
  constructor(...args) {
    super(...args, { name: 'purge', enabled: true });
  }

  async run({ channel }) {
    const _channel = await this.client.channels.get(channel);
    let logChannel = _channel.guild.settings.get('channels.logs');
    let messages = await _channel.messages.fetch({ limit: 100 });
    let purged = 0;

    const t0 = performance.now();
    while (messages.size > 0) {
      await _channel.bulkDelete(messages);
      purged += messages.size;
      messages = await _channel.messages.fetch({ limit: 100 });
    }
    const t1 = performance.now();

    if (!logChannel) return;
    logChannel = _channel.guild.channels.get(logChannel);
    logChannel.send(`Purged \`${purged}\` messages in ${_channel}.\n\`Took ${(t1 - t0).toFixed(0) / 1000}s\``);
  }
};
