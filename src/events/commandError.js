const { Event } = require('klasa');

module.exports = class extends Event {
  run(msg, command, params, error) {
    if (error instanceof Error) this.client.emit('wtf', `[COMMAND] ${command.path}\n${error.stack || error}`);
    if (error.message) msg.sendCode('JSON', error.message).catch((err) => this.client.emit('wtf', err));
    else msg.send(error).catch((err) => this.client.emit('wtf', err));
  }
};
